<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $label;

    //Si on voulait faire avec une regex : [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Ip()
     */
    private $ip;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\LessThanOrEqual(100)
     * @Assert\GreaterThanOrEqual(0)
     */
    private $battery;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $os;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Appli", mappedBy="devices")
     */
    private $applis;

    public function __construct()
    {
        $this->applis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getBattery(): ?int
    {
        return $this->battery;
    }

    public function setBattery(?int $battery): self
    {
        $this->battery = $battery;

        return $this;
    }

    public function getOs(): ?string
    {
        return $this->os;
    }

    public function setOs(?string $os): self
    {
        $this->os = $os;

        return $this;
    }

    /**
     * @return Collection|Appli[]
     */
    public function getApplis(): Collection
    {
        return $this->applis;
    }

    public function addAppli(Appli $appli): self
    {
        if (!$this->applis->contains($appli)) {
            $this->applis[] = $appli;
            $appli->addDevice($this);
        }

        return $this;
    }

    public function removeAppli(Appli $appli): self
    {
        if ($this->applis->contains($appli)) {
            $this->applis->removeElement($appli);
            $appli->removeDevice($this);
        }

        return $this;
    }
}
