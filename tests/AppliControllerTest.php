<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use App\DataFixtures\AppFixtures;

class AppliControllerTest extends WebTestCase
{
    public function setUp() {
        self::bootKernel();
        //On peut également récupérer l'ObjectManager comme ça, après avoir
        //lancé la méthode bootKernel()
        $manager = self::$container->get('doctrine.orm.entity_manager');
        //L'ORMPurger va nous permettre de vider la bdd avant de relancer la fixture
        $purger = new ORMPurger($manager);
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE appli AUTO_INCREMENT = 1;");
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }

    public function testGetAll()
    {
        $client = static::createClient();
        $client->request('GET', '/appli');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(5, $data);
        $this->assertSame('Appli 1', $data[0]['name']);

    }

    public function testAddSuccess() {
        $client = static::createClient();

        $client->request('POST', '/appli', [], [], [], json_encode([
            "name" => "test",
            "size" => 100,
            "lastUpdate" => "2019-10-10",
            "downloads" => 10
        ]));

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\AppliRepository');
        $this->assertSame(6, $repo->count([]));
    }

    public function testGetOneApp()
    {
        $client = static::createClient();
        $client->request('GET', '/appli/1'); 

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame(1, $data['id']);
        $this->assertSame("Appli 1", $data['name']);
    }

    public function testDeleteApp()
    {
        $client = static::createClient();
        
        $client->request('DELETE', '/appli/1');

        $this->assertSame(204, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\AppliRepository');
        $this->assertSame(4, $repo->count([]));
    }

    public function testUpdateSuccess() {
        $client = static::createClient();

        $client->request('PATCH', '/appli/1', [], [], [], json_encode([
            "name" => "test",
            "size" => 100,
            "lastUpdate" => "2019-10-10",
            "downloads" => 10
        ]));

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\AppliRepository');
        $this->assertSame(5, $repo->count([]));
        $appli = $repo->find(1);
        $this->assertSame('test', $appli->getName());
        $this->assertSame(100, $appli->getSize());

    }
}
