<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Device;
use App\Entity\Appli;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($x = 1; $x <= 5; $x++) {
            $device = new Device();
            $device->setLabel('Device '.$x);
            $device->setOs('Linux');
            $device->setBattery($x*10);
            $device->setIp('192.168.1.'.$x);
            $manager->persist($device);

            $appli = new Appli();
            $appli->setName('Appli ' .$x);
            $appli->setSize($x*10);
            $appli->setDownloads($x);
            $appli->setLastUpdate(new \DateTime('2019-04-0'.$x));
            $appli->addDevice($device);
            $manager->persist($appli);

        }

        $manager->flush();
    }
}
