<?php

namespace App\Form;

use App\Entity\Appli;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AppliType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('lastUpdate', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('size')
            ->add('downloads')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Appli::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
