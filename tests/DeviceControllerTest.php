<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\DataFixtures\AppFixtures;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class DeviceControllerTest extends WebTestCase
{
    public function setUp() {
        self::bootKernel();
        //On peut également récupérer l'ObjectManager comme ça, après avoir
        //lancé la méthode bootKernel()
        $manager = self::$container->get('doctrine.orm.entity_manager');
        //L'ORMPurger va nous permettre de vider la bdd avant de relancer la fixture
        $purger = new ORMPurger($manager);
        
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE device AUTO_INCREMENT = 1;");
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }

    public function testGetAll()
    {
        
        $client = static::createClient();
        $client->request('GET', '/device');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        //La route en question nous renvoie du json, on peut donc décoder ce
        //json pour en faire un array associatif sur lequel on pourra faire nos
        //assertions plus simplement
        $data = json_decode($client->getResponse()->getContent(), true);
        //On vérifie qu'on a bien 5 items
        $this->assertCount(5, $data);
        //On vérifie que la valeur du label du premier item correspond à ce qu'on a mis dans la fixture
        $this->assertSame("Device 1", $data[0]['label']);
    }

    public function testAddSuccess() {
        $client = static::createClient();

        $client->request('POST', '/device', [], [], [], json_encode([
            "label" => "test",
            "ip" => "192.1.1.1",
            "os" => "Android",
            "battery" => 10
        ]));

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(6, $repo->count([]));
    }

    public function testGetOne() {
        $client = static::createClient();
        $client->request('GET', '/device/1');

        $this->assertSame(200, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame(1, $data['id']);
        $this->assertSame("Device 1", $data['label']);
        
    }

    public function testRemove() {
        $client = static::createClient();
        $client->request('DELETE', '/device/1');

        $this->assertSame(204, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(4, $repo->count([]));
    }

    public function testUpdateSuccess() {
        $client = static::createClient();

        $client->request('PATCH', '/device/1', [], [], [], json_encode([
            "label" => "test",
            "ip" => "192.1.1.1",
            "battery" => 10
        ]));

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(5, $repo->count([]));
        $device = $repo->find(1);
        $this->assertSame('test', $device->getLabel());
        $this->assertSame('Linux', $device->getOs());

    }

    /*
    //ok...
     private function doRequest($method, $url, $code) {
        $client = static::createClient();
        $client->request($method, $url);

        $this->assertSame($code, $client->getResponse()->getStatusCode());
        return $client;
    }*/
}
