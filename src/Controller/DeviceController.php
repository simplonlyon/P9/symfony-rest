<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use JMS\Serializer\SerializerInterface;
use App\Repository\DeviceRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Device;
use App\Form\DeviceType;

/**
 * @Route("/device", name="device")
 */
class DeviceController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * On injecte le JMS\Serializer dans le constructeur, car on en aura
     * besoin dans toutes les méthodes de ce contrôleur
     */
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }
    /**
     * @Route(methods="GET")
     */
    public function index(DeviceRepository $repo)
    {
        //On va chercher les devices comme d'hab
        $devices = $repo->findAll();
        //On les convertit en json avec le jms serializer
        $json = $this->serializer->serialize($devices, 'json');

        // return new Response(json_encode(["ga" => "bloup"]), 200, ['Content-Type'=> 'application/json']);
        // return $this->json(["ga" => "bloup"]);

        //On envoie le json dans une JsonResponse avec un code http à 200 (success)
        //pas de header Http, et en lui disant que ce qu'on lui donne c'est déjà du json
        return new JsonResponse($json, 200, [], true);

    }
    /**
     * @Route(methods="POST")
     */
    public function add(Request $request, ObjectManager $manager) {
        $device = new Device();        
        $form = $this->createForm(DeviceType::class, $device);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($device);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($device, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);

    }

    /**
     * @Route("/{device}", methods="GET")
     */
    public function one(Device $device) {
        return new JsonResponse($this->serializer->serialize($device, 'json'), 200, [], true);
    }
    
    /**
     * @Route("/{device}", methods="DELETE")
     */
    public function remove(Device $device, ObjectManager $manager) {
        $manager->remove($device);
        $manager->flush();
        return $this->json('', 204);
    }

    /**
     * @Route("/{device}", methods="PATCH")
     */
    public function update(Device $device, ObjectManager $manager, Request $request) {
        $form = $this->createForm(DeviceType::class, $device);

        $form->submit(json_decode($request->getContent(), true), false);

        if($form->isSubmitted() && $form->isValid()) {
            
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($device, 'json'), 200, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    }


    
}
